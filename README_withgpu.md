# Docker container images with "headless" VNC session + Python3.6 + Pip3 + Tensorflow with cpu & gpu + Openaigym

Modified (inspired from): https://github.com/ConSol/docker-headless-vnc-container

------------------------
INSTALL nvidia-docker
------------------------

The repository contains a collection of Docker images with headless VNC environments.

The Docker image is installed with the following components:

* Desktop environment [**Xfce4**](http://www.xfce.org)
* VNC-Server (default VNC port `5901`)
* [**noVNC**](https://github.com/novnc/noVNC) - HTML5 VNC client (default http port `6901`)
  
![Docker VNC Desktop access via HTML page](.pics/vnc_container_view.png)



## Usage
For instance see following the usage of the image:

Print out help page:

    nvidia-docker run image_name --help    

Run command with mapping to local port `5901` (vnc protocol) and `6901` (vnc web access):

    nvidia-docker run -d -p 5901:5901 -p 6901:6901 image_name
  
Change the default user and group within a container to your own with adding `--user $(id -u):$(id -g)`:

    nvidia-docker run -d -p 5901:5901 -p 6901:6901 --user $(id -u):$(id -g) image_name

If you want to get into the container use interactive mode `-it` and `bash`     

    nvidia-docker run -it -p 5901:5901 -p 6901:6901 image_name bash

Build an image from scratch:

    nvidia-docker build -t tag_name path_to_docker_file 

# Connect & Control
If the container is started like mentioned above, connect via one of these options:

* connect via __VNC viewer `localhost:5901`__, default password: `vncpassword`
* connect via __noVNC HTML5 full client__: [`http://localhost:6901/vnc.html`](http://localhost:6901/vnc.html), default password: `vncpassword` 
* connect via __noVNC HTML5 lite client__: [`http://localhost:6901/?password=vncpassword`](http://localhost:6901/?password=vncpassword) 


## Hints

### 1) Extend a Image with your own software
Since `1.1.0` all images run as non-root user per default, so that means, if you want to extend the image and install software, you have to switch in the `Dockerfile` back to the `root` user:

```bash
## Custom Dockerfile
FROM consol/centos-xfce-vnc
ENV REFRESHED_AT 2018-03-18

## Install a gedit
USER 0
RUN yum install -y gedit \
    && yum clean all
## switch back to default user
USER 1000
```

### 2) Change User of running Sakuli Container

Per default, since version `1.3.0` all container processes will executed with user id `1000`. You can change the user id like follow: 

#### 2.1) Using root (user id `0`)
Add the `--user` flag to your docker run command:

    nvidia-docker run -it --user 0 -p 6911:6901 image_name

#### 2.2) Using user and group id of host system
Add the `--user` flag to your docker run command:

    nvidia-docker run -it -p 6911:6901 --user $(id -u):$(id -g) image_name

### 3) Override VNC environment variables
The following VNC environment variables can be overwritten at the `docker run` phase to customize your desktop environment inside the container:
* `VNC_COL_DEPTH`, default: `24`
* `VNC_RESOLUTION`, default: `1280x1024`
* `VNC_PW`, default: `my-pw`

#### 3.1) Example: Override the VNC password
Simply overwrite the value of the environment variable `VNC_PW`. For example in
the docker run command:

    nvidia-docker run -it -p 5901:5901 -p 6901:6901 -e VNC_PW=my-pw image_name

#### 3.2) Example: Override the VNC resolution
Simply overwrite the value of the environment variable `VNC_RESOLUTION`. For example in
the docker run command:

    nvidia-docker run -it -p 5901:5901 -p 6901:6901 -e VNC_RESOLUTION=800x600 image_name
    
### 4) View only VNC
Since version `1.2.0` it's possible to prevent unwanted control over VNC. Therefore you can set the environment variable `VNC_VIEW_ONLY=true`. If set the docker startup script will create a random cryptic password for the control connection and use the value of `VNC_PW` for the view only connection over the VNC connection.

     nvidia-docker run -it -p 5901:5901 -p 6901:6901 -e VNC_VIEW_ONLY=true image_name


