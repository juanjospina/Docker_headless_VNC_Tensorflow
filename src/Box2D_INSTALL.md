
### Building from source: Linux
--------------------------------
1. Download and install the latest version of SWIG (preferably 3.0+) with 
   your package manager.

   If you are using Ubuntu, you can install it via Synaptic Package Manager
   (package name 'swig'). You will also need to install the python-dev package,
   and build-essential. Finally, you'll need python-pygame if you want to run
   the testbed.

    ```bash
    $ sudo apt-get install build-essential python-dev swig python-pygame git
    ```

2. Clone the git repository

    ```bash
    $ git clone https://github.com/pybox2d/pybox2d
    ```

3. Build and install the pybox2d library

    ```bash
    $ python setup.py build

    # Assuming everything goes well...
    $ sudo python setup.py install
    ```


    # Assuming you're using either Python from conda or brew Python,
    # you should be able to install without superuser privileges:
    $ python setup.py install

    # For development purposes, it may be more convenient to do a develop
    # install instead
    $ python setup.py develop
    ```
